package com.joncasagrande.talkchallange.model

class Phone(var rawNumber : String) {

    private val INVALID_CODE =-1
    private val MIN_PHONE_SIZE = 3
    private val NUMBER_SIZE = 9

    private var size : Int = rawNumber.length

    var areaCode : Int = 0
    val number : String

    init {

        val aCode = extractAreaCode()
        if(aCode >= 1){
            areaCode = aCode
            number = rawNumber.substring(size-NUMBER_SIZE, size)
        }else{
            number = rawNumber
        }

    }

    fun extractAreaCode():Int{
        if(rawNumber.isNullOrEmpty()) return INVALID_CODE
        removeZeroOrPlus()
        if(!hasOnlyNumbers() || !hasValidSize()) return INVALID_CODE
        if(rawNumber.length == MIN_PHONE_SIZE) return 0



        return rawNumber.substring(0,size- NUMBER_SIZE).toInt()
    }

    private fun removeZeroOrPlus(){
        if(size > 3) {
            if (rawNumber[0] == '+') {
                rawNumber = rawNumber.substring(1, size)
            } else if ((rawNumber[0] == '0' && rawNumber[1] == '0')) {
                rawNumber = rawNumber.substring(2, size)
            }
            size = rawNumber.length
        }
    }

    private fun hasValidSize(): Boolean{
        return  size  == 3 || (size in 8..12)
    }

    private fun hasOnlyNumbers():Boolean{
        val regexWithPlus = Regex("(\\+([0-9]*))")
        val regex = Regex("([0-9]*)")
        return rawNumber.matches(regexWithPlus) || rawNumber.matches(regex)
    }
}