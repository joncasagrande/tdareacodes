import com.joncasagrande.talkchallange.model.Phone
import java.io.File

class Main {

    lateinit var  mapAreas : HashMap<Int,Int>

    fun readAreaCodeFile(filePath : String){
        mapAreas = readAreaCode("./src/java/areacodes.txt")
        if(!mapAreas.isEmpty()) {
            countOccurrences(filePath)
            print()
        }else{
            System.out.println("Invalid file path for area code")
        }

    }

    fun readAreaCode(fileName: String): HashMap<Int, Int>{
        val file = File(fileName)
        val map = HashMap<Int, Int>()
        if(file.exists()){
            file.readLines().forEach {
                val areaCode = it.toInt()
                if (!map.containsKey(areaCode)){
                    map.put(areaCode, 0)
                }
            }
        }
        return map
    }

    fun countOccurrences(fileName: String){
        val file = File(fileName)
        if(file.exists()){
            file.readLines().forEach {
                val phone = Phone(it)
                if (mapAreas.containsKey(phone.areaCode)){
                    mapAreas.put(phone.areaCode, mapAreas.getValue(phone.areaCode)+1)
                }
            }
        }

    }

    private fun print() {
        mapAreas.filter { it.value > 0 }.toSortedMap().forEach{
        System.out.println("${it.key} : ${it.value}")
        }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {

            val main = Main()
            if(!args.isEmpty()){
                main.readAreaCodeFile(args.get(0))
            }
        }
    }
}