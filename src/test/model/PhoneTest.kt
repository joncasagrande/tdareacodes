package com.joncasagrande.talkchallange.model



/**
 * Testing the parse between the string and the area code
 *
 * Validation that must be checked
 * - has either 3 digits or between 7 and 12 digits (inclusive)
- can have the optional '+' character in the beginning (before any digit)
- can start with '00', in which case it shouldn't start with the '+' sign
- if it starts with '00', these two digits don't count to the maximum number of digits
- cannot have any letters --DONE
- cannot have any symbol aside from the beginning '+' sign -- DONE
- cannot have any whitespace between the '+' sign and the first digit but can have any amount of whitespace in all other places --DONE


class PhoneTest {


    lateinit var phone : Phone

    @Test
    fun testExtractAreaCodeInEmptyNumber(){
        //given phone is empty
        phone = Phone("")
        //when
        val code = phone.extractAreaCode()
        //then -- should return -1
        assertEquals("Should return -1", -1,code)
    }
    @Test
    fun testExtractAreaCodeInInvalidSize(){
        //given phone is empty
        phone = Phone("1111")
        //when
        val code = phone.extractAreaCode()
        //then -- should return -1
        assertEquals("Should return -1", -1,code)
    }

    @Test
    fun testExtractAreaCodeWithSmalNumber(){
        //given
        phone = Phone("112")
        //when
        val code = phone.extractAreaCode()
        //then -- should return 0
        assertEquals("Should return 0", 0,code)
    }

    @Test
    fun testExtractAreaCodeWithSmalNumberWithPlus(){
        //given
        phone = Phone("+112")
        //when
        val code = phone.extractAreaCode()
        //then -- should return 0
        assertEquals("Should return 0", 0,code)
    }

    @Test
    fun testExtractAreaCodeWithSmalNumberWithZeros(){
        //given
        phone = Phone("00112")
        //when
        val code = phone.extractAreaCode()
        //then -- should return 0
        assertEquals("Should return 0", 0,code)
    }

    @Test
    fun testExtractAreaCodeWithMoreThanMaxSizeOfTheNumber(){
        //given
        phone = Phone("0055319999999999")
        //when
        val code = phone.extractAreaCode()
        //then -- should return -1
        assertEquals("Should return -1", -1,code)
    }


    @Test
    fun testExtractAreaCodeWithPlusButMoreThanMaxSizeNumber(){
        //given
        phone = Phone("+55319999999999")
        //when
        val code = phone.extractAreaCode()
        //then -- should return -1
        assertEquals("Should return -1", -1,code)
    }

    @Test
    fun testExtractAreaCodeWithZerosInNumber(){
        //given
        phone = Phone("0055999999999")
        //when
        val code = phone.extractAreaCode()
        //then -- should return -1
        assertEquals("Should return 55", 55,code)
    }


    @Test
    fun testExtractAreaCodeWithPlusInTheNumber(){
        //given
        phone = Phone("+55999999999")
        //when
        val code = phone.extractAreaCode()
        //then -- should return -1
        assertEquals("Should return 55", 55,code)
    }

    @Test
    fun testExtractAreaCodeLetters(){
        //given
        phone = Phone("A12")
        //when
        val code = phone.extractAreaCode()
        //then -- should return -1
        assertEquals("Should return -1", -1,code)
    }

    @Test
    fun testExtractAreaWithOnlyZeros(){
        //given
        phone = Phone("000")
        //when
        val code = phone.extractAreaCode()
        //then -- should return 0
        assertEquals("Should return 0", 0,code)
    }

    @Test
    fun testExtractAreaWithOnlyEspaceBetweenPlusAndNumbers(){
        //given
        phone = Phone("+ 000")
        //when
        val code = phone.extractAreaCode()
        //then -- should return 0
        assertEquals("Should return -1", -1,code)
    }
}
 */